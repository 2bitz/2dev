<?

class Controller {

	public $module;

	public function __construct(){
		global $System;

		$controllerName = get_class($this);
		if($this->module = $System->modules->getModuleByController($controllerName)){
			$this->setModels();
		}
	}

	private function setModels(){

		foreach ($this->module->models as $model) {
			if(class_exists($model->className)){
				$this->{$model->className} = new $model->className();
			}			
		}
	}

	public function loadView($fileName, $vars = null){

		foreach ($this->module->views as $view) {
			if($view->fileName == $fileName){
				require_once $view->filePath;
			}
						
		}
	}
}

?>