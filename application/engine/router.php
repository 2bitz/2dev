<?

class Router {

	private $routes = array();
	private $currentRoute = null;

	public function add($route){
		$this->addRoute($route);
	}

	public function addRoute($route){
		if($route instanceof Route){
			$this->routes[] = $route;
		}
	}

	private function setCurrentRoute($route){
		if($route instanceof Route){
			$this->currentRoute = $route;
		}
	}

	private function importRoutes(){
		require_once "/routes.php";
	}

	public function init(){
		global $System;

		$this->importRoutes();
		$uri = $System->util->getURI();

		if(count($this->routes) <= 0){
			throw new RouteException("No routes created");
			
		}
		foreach ($this->routes as $route) {
			if($route->match($uri)){
				$this->currentRoute = $route;
				$route->go($uri);
				break;
			}
		}

		if($this->currentRoute == null){
			throw new RouteException("No route matches the current URL");
		}
	}

	public function file($filepath){
		$a = explode('.', $filepath);
		$ext = $a[count($a)-1];

		$mimeType = [
			'html' => 'text/html',
			'js' => 'text/javascript',
			'css' => 'text/css',
			'json' => 'application/json',
			'xml' => 'application/xml',
			'jpg' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'gif' => 'image/gif',
			'zip' => 'application/zip',
		];

		header("Content-type: " . $mimeType[$ext]);
		echo file_get_contents($filepath);
	}

} 


/*
=================================================================
*/

class Route {

	public $route, $pattern, $function; 

	public function __construct($route, $function){
		$this->route = $route;
		$this->pattern = $this->patternfy($route);	
		$this->function = $function;
	}

	public function match($uri){
		return preg_match($this->pattern, $uri);
	}

	public function go($uri){
		if(preg_match($this->pattern, $uri, $matches)){

			$fn = $this->function;

			$params = array();
			if(is_array($matches)){
				for ($kk=1; $kk<count($matches); $kk++) { 
					$params[] = sprintf('"%s"', addslashes($matches[$kk]));
				}
			}

			$textParams = implode(',', $params);
			eval('$fn('.$textParams.');');
		}
	}

	private function patternfy($url){
		$pattern = $url;
		$pattern = preg_replace("/\{[a-zA-Z0-9]*\}/", "%VAR%", $pattern);

		preg_match_all("/\([a-zA-Z0-9\.\-\_\|\*\^\$\[\]\{\}\&\%\<\>\:\;\!]*\)/", $pattern, $matches);
		$changeIndex = array();
		if(is_array($matches[0])){
			foreach ($matches[0] as $key => $value) {
				$mark = "[".md5(uniqid().rand())."]";
				$changeIndex[] = [$mark, $value];
				$pattern = str_replace($value, $mark, $pattern);
			}
		}

		$pattern = strtr($pattern, [
			"/" => "\/", "." => "\.", "-" => "\-"
		]);

		if(is_array($changeIndex)){
			foreach ($changeIndex as $change) {
				$pattern = str_replace($change[0], $change[1], $pattern);
			}
		}

		$pattern = str_replace("%VAR%", "([a-zA-Z0-9\.\-\_]+)", $pattern);

		return sprintf("/^%s$/", $pattern);
	}

}
?>