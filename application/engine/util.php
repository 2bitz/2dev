<?

class Util {

	public function getIniData($path, $lowercase = false){
        if(file_exists($path)){
            $content = file_get_contents($path);
            $lines = explode(chr(10), $content);

            if(is_array($lines)){
    	        $data = [];
            	foreach ($lines as $index => $line) {
            		if(substr($line, 0, 1) != '#' and trim($line) != ''){
            			$parts = explode('=', $line);
            			$var = trim(str_replace(' ', '', ($parts[0])));
            			$value = trim(substr($parts[1], 0, 1) == ' ' ? substr($parts[1], 1) : $parts[1]);
                        if($lowercase === true){
                			$data[strtolower($var)] = $value;
                        } else {
                            $data[$var] = $value;
                        }
            		}
            	}
            }
            return $data;
        } else {
            return null;
        }

	}

    public function getURI($full=false){
        if($full === true){
            return $_SERVER['REQUEST_URI'];
        } else {
            $uri = explode("?", substr($_SERVER['REQUEST_URI'], 1));
            return $uri[0];
        }
    }

    public function getDirectoryItems($path){
        if(file_exists($path)){
            $items = array();
            if ($rootHandler = opendir($path)) {
                while (false !== ($rootFile = readdir($rootHandler))) {
                    if ($rootFile != "." and $rootFile != ".." ) {
                        $items[] = $rootFile;
                    }
                }
            }
            return $items;
        } else {
            return null;
        }
    }

    public function getDirectoryFiles($path){
        if($items = $this->getDirectoryItems($path)){
            $files = array();
            foreach ($items as $item) {
                if(is_file(sprintf("%s/%s", $path, $item))){
                    $files[] = $item;
                }
            }
            return $files;
        } else {
            return null;
        }
    }

    public function getDirectoryFolders($path){
        if($items = $this->getDirectoryItems($path)){
            $folders = array();
            foreach ($items as $item) {
                if(is_dir(sprintf("%s/%s", $path, $item))){
                    $folders = $item;
                }
            }
            return $folders;
        } else {
            return null;
        }
    }
}
?>