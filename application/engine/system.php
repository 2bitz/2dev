<?

class System {

	public $router, $session, $util, $database, $configuration;

	public function __construct(){
		$this->router = new Router();
		$this->session = new Session();
		$this->util = new Util();
		$this->database = new Database();
		$this->configuration = new Configuration();
		$this->modules = new Modules();
	}

	

}

?>