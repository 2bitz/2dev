<?

require_once "/../application/engine/system.php";
require_once "/../application/engine/database.php";
require_once "/../application/engine/router.php";
require_once "/../application/engine/session.php";
require_once "/../application/engine/util.php";
require_once "/../application/engine/exception.php";
require_once "/../application/engine/configuration.php";
require_once "/../application/engine/modules.php";

require_once "/../application/layers/controller.php";
require_once "/../application/layers/model.php";

class App {

	public function __construct(){
		global $System;
		$System = new System();
	}

	public function start(){
		global $System;

		try {

			$System->configuration->init();
			$System->database->init();
			$System->modules->init();
			$System->router->init();

		} catch(DatabaseException $e) {

			echo sprintf("Database Exception: %s", $e->getMessage());

		} catch(RouteException $e) {

			echo sprintf("Route Exception: %s", $e->getMessage());

		} catch(Exception $e) {

			echo sprintf("General Exception: %s", $e->getMessage());

		}
	}

}

?>