<?

require_once "/../resourses/library/2db/2db.php";

class Database extends ToDB {
	
	public function init(){
		global $System;

		$this->setup(
			$System->configuration->get('database.server'),
			$System->configuration->get('database.username'),
			$System->configuration->get('database.password'),
			$System->configuration->get('database.schema'),
			$System->configuration->get('database.charset')
		);
	}
}

?>