<?

/*
=================================================================
	MODULE CONTROLLER
=================================================================
*/
class Modules {

	private $modules = array(), $live;

	public function init(){
		global $System;
		$path = $System->configuration->get('app.modules.path');
		$this->search($path);
	}

	private function search($path){
		global $System;

		if($items = $System->util->getDirectoryItems($path)){
			foreach ($items as $item) {
				$module = new Module(sprintf("%s/%s", $path, $item));
				if($module->ready()){
					$this->register($module);
				}
			}
		}
	}

	private function register($module){
		if($module instanceof Module){
			$this->modules[] = $module;
		}
	}

	public function run($module, $action = null, $params = null){
		if(!$module = $this->getModuleByName($module)){
			throw new Exception("The requested module doesn't exits");
		}

		$this->requireFiles();

		if(!class_exists($module->mainController->className)){
        	throw new Exception("Controller doesn't exists");
        }

        $action = $action == null ? 'default' : $action;
        if(!method_exists($module->mainController->className, '_'.$action)){
        	throw new Exception("Controller doesn't have the required method");
        }

        $this->live = new $module->mainController->className;
        $this->live->{sprintf("_%s", $action)}($params);


	}

	public function getModuleByName($name){
		foreach ($this->modules as $module) {
			if($module->name == $name){
				return $module;
			}
		}
		return null;
	}

	public function getModuleByController($class){
		foreach ($this->modules as $module) {
			foreach ($module->controllers as $controller) {
				if($controller->className == $class){
					return $module;
				}
			}
		}
		return null;
	}

	public function requireFiles(){
		foreach ($this->modules as $module) {
			foreach ($module->controllers as $controller) {
				require_once $controller->filePath;
			}
			foreach ($module->models as $model) {
				require_once $model->filePath;
			}
		}
	}

}



/*
=================================================================
	MODULE OBJECT
=================================================================
*/
class Module {

	public $name, $rootPath, $mainController;
	public $controllers = array(), $models = array(), $views = array(), $permissions = array();

	public function __construct($path){
		$this->rootPath = $path;
		$this->controllerPath = sprintf("%s/controller", $path);
		$this->modelPath = sprintf("%s/model", $path);
		$this->viewPath = sprintf("%s/view", $path);

		$this->setup();
	}

	private function setup(){
		try {
			$this->getProperties();
			$this->getControllers();
			$this->getModels();
			$this->getViews();
			$this->getMainController();
		} catch (Exception $e) {
			
		}
	}

	public function ready(){
		return (
			($this->mainController instanceof ControllerObject) and
			(count($this->controllers) > 0) and
			(count($this->models) > 0)
		);
	}

	private function getProperties(){
		$properties = $this->getIniContent();
		$this->name = $properties->name;
		$this->permissions = $properties->permissions;
	}

	private function getControllers(){
		global $System;
		if($controllers = $System->util->getDirectoryFiles($this->controllerPath)){
			foreach ($controllers as $controller) {
				$this->controllers[] = new ControllerObject($this->controllerPath, $controller);
			}
		}		
	}

	private function getModels(){
		global $System;
		if($models = $System->util->getDirectoryFiles($this->modelPath)){
			foreach ($models as $model) {
				$this->models[] = new ModelObject($this->modelPath, $model);
			}
		}
	}

	private function getViews(){
		global $System;
		if($views = $System->util->getDirectoryFiles($this->viewPath)){
			foreach ($views as $view) {
				$this->views[] = new ViewObject($this->viewPath, $view);
			}
		}
	}

	private function getIniContent(){
		global $System;
		if(!$content = $System->util->getIniData(sprintf("%s/register.ini", $this->rootPath), true)){
			throw new Exception("Doesn't have ini file", 1);
		}
		return (object)$content;
	}

	public function getMainController(){
		foreach ($this->controllers as $controller) {
			if($controller->className == $this->getIniContent()->mainclass){
				$this->mainController = $controller;
			}
		}
	}
}



/*
=================================================================
	CONTROLLER OBJECT
=================================================================
*/
class ControllerObject {

	public $className, $filePath, $fileName;

	public function __construct($path, $fileName){
		$this->filePath = sprintf("%s/%s", $path, $fileName);
		$this->fileName = $fileName;

		$this->setup();		
	}

	private function setup(){
		$content = file_get_contents($this->filePath);
		$lines = explode(chr(10), $content);
		foreach ($lines as $line) {
			if(preg_match("/class(\ +)([A-Za-z0-9\_]+)(\ +)extends(\ +)Controller(\ *)/", $line, $matches)){
				$this->className = $matches[2];
			}
		}
	}
}



/*
=================================================================
	MODEL OBJECT
=================================================================
*/
class ModelObject {

	public $className, $filePath, $fileName;

	public function __construct($path, $fileName){
		$this->filePath = sprintf("%s/%s", $path, $fileName);
		$this->fileName = $fileName;

		$this->setup();		
	}

	private function setup(){
		$content = file_get_contents($this->filePath);
		$lines = explode(chr(10), $content);
		foreach ($lines as $line) {
			if(preg_match("/class(\ +)([A-Za-z0-9\_]+)(\ +)extends(\ +)Model(\ *)/", $line, $matches)){
				$this->className = $matches[2];
			}
		}
	}
}



/*
=================================================================
	VIEW OBJECT
=================================================================
*/
class ViewObject {

	public $name, $filePath, $fileName;

	public function __construct($path, $fileName){
		$this->name = str_replace(".view.php", "", $fileName);
		$this->filePath = sprintf("%s/%s", $path, $fileName);
		$this->fileName = $fileName;
	}
}
?>