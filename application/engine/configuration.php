<?

class Configuration {

	private $configs = array();
	private $defaults = array(
		'app.modules.path' => '../application/modules',

		'database.server' => 'localhost',
		'database.username' => 'root',
		'database.password' => '',
		'database.schema' => 'default',
		'database.charset' => 'utf8',
	);

	public function set($key, $value){
		$this->configs[$key] = $value;
	}

	public function get($key){
		return $this->configs[$key] !== null ? $this->configs[$key] : $this->defaults[$key];
	}

	public function getDefault($key){
		return $this->defaults[$key];
	}

	public function init(){
		$this->importConfig();
	}

	private function importConfig(){
		require_once "/config.php";
	}
}

?>