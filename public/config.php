<?
/*
CONFIGURATION FILE
All the application configuration data goes on this file
*/

// APPLICATION
$this->set('app.key', md5('My App Name'));
$this->set('app.charset', 'UTF-8');
$this->set('app.timezone', 'America/Sao_Paulo');
$this->set('app.language', 'en-us');


// DATABASE
$this->set('database.server', 'localhost');
$this->set('database.username', 'root');
$this->set('database.password', '');
$this->set('database.schema', '');
$this->set('database.charset', 'utf8');

?>