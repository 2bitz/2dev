<?php

$p1 = (isset($argv[1]) ? $argv[1] : null);
$p2 = (isset($argv[2]) ? $argv[2] : null);
$p3 = (isset($argv[3]) ? $argv[3] : null);
$p4 = (isset($argv[4]) ? $argv[4] : null);

try {
	if(is_null($action = $p1)){
		throw new Exception("The action ain't set");
	}
	switch ($action) {
		case 'module':
			$module = new Module($p2);
			$module->create();
			break;
		
		default:
			throw new Exception(sprintf("The maker doesn't have the action '%s'", $action));
			break;
	}

} catch(Exception $e) {
	echo $e->getMessage();
}
die('Done');

class Module {

	public $name, $modulesPath = "./application/modules", $moduleDirectory;

	public function __construct($name){
		if(is_null($name)){
			throw new Exception("The module name can't be empty");
		}
		$this->name = $name;
	}

	public function create(){
		echo sprintf("Creating module \"%s\"...\n", $this->name);

		$this->moduleDirectory = $this->modulesPath.'/'.$this->name;
		if(file_exists($this->moduleDirectory)){
			throw new Exception(sprintf("The module \"%s\" already exists\n", $this->name));
		}
		mkdir($this->moduleDirectory);

		$this->createController();
		$this->createModel();
		$this->createIni();
		
		echo sprintf("Module \"%s\" created sucessfull\n", $this->name);
	}

	private function createController(){
		try {
			$controllerDirectory = sprintf("%s/controller", $this->moduleDirectory);
			if(file_exists($controllerDirectory)){
				throw new Exception("Controller directory already exists");
			}
			if(!mkdir($controllerDirectory)){
				throw new Exception("Can't create controller directory");
			}
			$className = ucfirst($this->name).'Controller';
			$content = "<?
class ".$className." extends Controller {

	public function _default(){

	}

}
?>";
			$fileName = sprintf("%s.controller.php", $this->name);
			$filePath = sprintf("%s/%s", $controllerDirectory, $fileName); 
			if(!file_put_contents($filePath, $content)){
				throw new Exception("Can't create controller file");
			}
		} catch (Exception $e) {
			throw new Exception(sprintf("Error creating the controller: %s", $e->getMessage()));
		}
	}

	private function createModel(){
		try {
			$modelDirectory = sprintf("%s/model", $this->moduleDirectory);
			if(file_exists($modelDirectory)){
				throw new Exception("Model directory already exists");
			}
			if(!mkdir($modelDirectory)){
				throw new Exception("Can't create model directory");
			}
			$className = ucfirst($this->name).'Model';
			$content = "<?
class ".$className." extends Model {

	public function listAll(){

	}

}
?>";
			$fileName = sprintf("%s.model.php", $this->name);
			$filePath = sprintf("%s/%s", $modelDirectory, $fileName); 
			if(!file_put_contents($filePath, $content)){
				throw new Exception("Can't create model file");
			}
		} catch (Exception $e) {
			throw new Exception(sprintf("Error creating the controller: %s", $e->getMessage()));
		}
	}

	public function createIni(){
		try {
			$content = "name = ".$this->name."
mainclass = ".ucfirst($this->name)."Controller
";
			$fileName = "register.ini";
			$filePath = sprintf("%s/%s", $this->moduleDirectory, $fileName); 
			if(!file_put_contents($filePath, $content)){
				throw new Exception("Can't create ini file");
			}
		} catch (Exception $e) {
			throw new Exception(sprintf("Error creating register.ini: %s", $e->getMessage()));
		}
	}

}

?>